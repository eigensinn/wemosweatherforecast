#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Wire.h>
#include <LCD_1602_RUS.h>
#include <ArduinoJson.h>

#ifndef STASSID
#define STASSID "YourWifiName"
#define STAPSK  "PwdForYourWifi"
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

String APIKEY = "YourApiKeyFromApi.openweathermap.org";
String CityID = "524901"; // Moscow (RU)
WiFiClient client;
char servername[] = "api.openweathermap.org";  // remote server we will connect to
String result;

String weatherDescription = "";
String weatherLocation = "";
String weatherMain = "";
String Country;
float Temperature;
float Humidity;
float Pressure;
int windSpeed;
int windDeg;
int cloudsAll;

int ledValue = 0;
const int buttonPin = 14;    // номер входа, подключенный к кнопке (D5 GPIO14)
const int carouselPin = 12;  // номер входа, подключенный к кнопке (D6 GPIO12)
const int refreshPin = 13;   // номер входа, подключенный к кнопке (D7 GPIO13) 
int buttonState = 0;         // переменная для хранения состояния кнопки
int carouselState = 0;
int refreshState = 0;

const unsigned long weatherTime = 5000UL;      /* Показываем погоду 5 секунд */
/* Начинается с 5000UL, потому что нужно 5 секунд на показ windAndCloudTime после обнуления таймера */
const unsigned long conditionsTime = 10000UL;   /* Показываем физические условия c 5 до 10 секунд */
const unsigned long windAndCloudTime = 15000UL; /* Показываем ветер и облака с 10 до 15 секунд */
const unsigned long getDataTime = 100UL; // Получаем данные первично
unsigned long previousMillis = 0;        // сохраняем время последнего обновления состояния millis()
unsigned long currentMillis = 0;
bool dataFlag = false; // Флаг наличия данных
int counter = 0; // Счетчик повтора демонстрации погоды
int carouselCounter = 0;

// Таймер дисплея
const unsigned long onTimeDisplay = 15000UL; /* Время активности дисплея 15 секунд */
unsigned long previousMillisDisplay = 0;
unsigned long currentMillisDisplay = 0;

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LCD_1602_RUS lcd(0x27, 16, 2);

void setup() {
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname("myesp8266");

  // No authentication by default
  ArduinoOTA.setPassword("yourPwd");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // initialize the LCD
  lcd.begin();
  lcd.backlight();
  
  // инициализируем пин, подключенный к кнопке, как вход
  pinMode(buttonPin, INPUT);
  pinMode(carouselPin, INPUT);
  pinMode(refreshPin, INPUT);
}

// Продлеваем время активности подсветки дисплея
void displayChecking() {
  if(lcd.getBacklight()) {
    previousMillisDisplay = currentMillisDisplay;
  }
}

void loop() {
  ArduinoOTA.handle();
  // Фиксируем счетчик времени
  currentMillis = millis();
  
  // Таймер дисплея
  currentMillisDisplay = millis();
  
  // Если дисплей включен и текущее значение таймера превышает 15 сек, отключаем дисплей
  if((lcd.getBacklight()) && (currentMillisDisplay - previousMillisDisplay >= onTimeDisplay)) {
    previousMillisDisplay = currentMillisDisplay;
    lcd.noBacklight();
  }
  
  if(dataFlag && (currentMillis - previousMillis == weatherTime)) {
    displayWeather(weatherLocation, weatherDescription);
  } else if (dataFlag && (currentMillis - previousMillis == conditionsTime)) {
    displayConditions(Temperature, Humidity, Pressure);
  } else if (dataFlag && (currentMillis - previousMillis == windAndCloudTime)) {
    displayWindAndCloud(windSpeed, windDeg, cloudsAll);
  } else if (!dataFlag && (currentMillis - previousMillis >= getDataTime)) {
    getWeatherData();               // Получаем данные
  } else if (dataFlag && (counter == 40)) {
    previousMillis = currentMillis; // Обнуляем таймер
    counter = 0; // Обнуляем счетчик
    dataFlag = false; // Устанавливаем флаг "Необходимо получить данные"
  }

  buttonState = digitalRead(buttonPin);

  if (buttonState == HIGH) {
    if (!lcd.getBacklight()) {
      // включаем подсветку дисплея
      lcd.backlight();
      delay(500);
    } else if (lcd.getBacklight()) {
      lcd.noBacklight();
      delay(500);
    }
  }

  carouselState = digitalRead(carouselPin);

  if (carouselState == HIGH) {
    displayChecking();
    switch (carouselCounter) {
      case 0:
        displayWeather(weatherLocation, weatherDescription);
        carouselCounter = 1;
        delay(500);
      break;
      case 1:
        displayConditions(Temperature, Humidity, Pressure);
        carouselCounter = 2;
        delay(500);
      break;
      case 2:
        displayWindAndCloud(windSpeed, windDeg, cloudsAll);
        carouselCounter = 0;
        delay(500);
      break;
    }
  }

  refreshState = digitalRead(refreshPin);

  if (refreshState == HIGH) {
    displayChecking();
    getWeatherData(); // Получаем данные
    delay(500);
  }
}

//client function to send/receive GET request data.
void getWeatherData() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(L"Получение данных");
  if (client.connect(servername, 80)) {  //starts client connection, checks for connection
    client.println("GET /data/2.5/weather?id="+CityID+"&units=metric&APPID="+APIKEY);
    client.println("Host: api.openweathermap.org");
    client.println("User-Agent: ArduinoWiFi/1.1");
    client.println("Connection: close");
    client.println();
  } 
  else {
    Serial.println("connection failed"); //error message if no client connect
    Serial.println();
  }

  while (client.connected() && !client.available()) {
    delay(1); //waits for data
  }
  while (client.connected() || client.available()) { //connected or data available
    result = client.readStringUntil('\n');
  }

  client.stop(); // stop client
  // Serial.println(result);

  // Пример ответа сервера
  /*{"coord":{"lon":37.62,"lat":55.75},
   "weather":[
    {"id":520,"main":"Rain","description":"light intensity shower rain","icon":"09d"},
    {"id":200,"main":"Thunderstorm","description":"thunderstorm with light rain","icon":"11d"}
   ],
   "base":"stations",
   "main":{"temp":18.92,"pressure":1012,"humidity":82,"temp_min":17,"temp_max":23.33},
   "visibility":10000,
   "wind":{"speed":4,"deg":210},
   "rain":{"1h":20.83},
   "clouds":{"all":75},
   "dt":1557410709,
   "sys":{"type":1,"id":9029,"message":0.005,"country":"RU","sunrise":1557365426,"sunset":1557422493},
   "id":524901,
   "name":"Moscow",
   "cod":200
  }
  [{"id":520,"main":"Rain","description":"light intensity shower rain","icon":"09d"},
   {"id":200,"main":"Thunderstorm","description":"thunderstorm with light rain","icon":"11d"}
  ]
  {"type":1,"id":9029,"message":0.005,"country":"RU","sunrise":1557365426,"sunset":1557422493}
  {"temp":18.92,"pressure":1012,"humidity":82,"temp_min":17,"temp_max":23.33}*/
  
  StaticJsonDocument<1024> root;
  auto error = deserializeJson(root, result);
  if (error) {
    Serial.println("deserializeJson() failed");
    Serial.println(error.c_str());
  }
  
  String location = root["name"];
  String country = root["sys"]["country"];
  float temperature = root["main"]["temp"];
  float humidity = root["main"]["humidity"];
  // Выбираем первую метеостанцию
  String weather = root["weather"][0]["main"];
  // Выбираем первую метеостанцию
  String description = root["weather"][0]["description"];
  float pressure = root["main"]["pressure"];
  float windspeed = root["wind"]["speed"];
  float winddeg = root["wind"]["deg"];
  float clouds = root["clouds"]["all"];
  
  weatherDescription = description;
  weatherLocation = location;
  weatherMain = weather;
  Country = country;
  Temperature = temperature;
  Humidity = humidity;
  Pressure = pressure;
  windSpeed = windspeed;
  windDeg = winddeg;
  cloudsAll = clouds;

  previousMillis = currentMillis; // Обнуляем таймер
  dataFlag = true; // Устанавливаем флаг "Данные получены"
}

void displayWeather(String weatherLocation, String weatherDescription) {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(weatherLocation);
  lcd.print(", ");
  lcd.print(Country);
  lcd.setCursor(0, 1);
  lcd.print(weatherDescription);
}

void displayConditions(float Temperature, float Humidity, float Pressure) {
  lcd.clear();
  lcd.print("T:"); 
  lcd.print(Temperature, 1);
  lcd.print((char)223);
  lcd.print("C ");
 
  // Printing Humidity
  lcd.print(L" В:");
  lcd.print(Humidity, 0);
  lcd.print(" %");
 
  // Printing Pressure
  lcd.setCursor(0, 1);
  lcd.print(L"Дв: ");
  lcd.print(Pressure, 1);
  lcd.print(L" гПа");
}

void displaySplash() {
  lcd.clear();
  lcd.print(L"Получение данных");
}

void displayWindAndCloud(int windSpeed, int windDeg, int cloudsAll) {
  lcd.clear();
  lcd.print(L"СВ:"); 
  lcd.print(windSpeed);
  lcd.print("m/s ");

  lcd.print(L" НВ:");
  lcd.print(windDeg);

  lcd.setCursor(0, 1);
  lcd.print(L"Обл: ");
  lcd.print(cloudsAll);
  lcd.print(" %");

  previousMillis = currentMillis; // Обнуляем таймер
  counter += 1; // Добавляем 1 к счетчику
}

# wemosWeatherForecast

Weather forecast wemos/lolin project. Wemos d1 mini, Wemos d1 DC-Power shield, LCD 1602(I2C), three 7-mm buttons.

License: [GPLv3](https://bitbucket.org/eigensinn/wemosweatherforecast/src/master/LICENSE.md).
